<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'muotopeite_db');

/** MySQL database username */
define('DB_USER', 'muotopeite_db');

/** MySQL database password */
define('DB_PASSWORD', 'sePQ7291PU');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IA!QuI|+3OTKnW4o|HT+%}L!@t@:Lu_]]< R2aa-P!3[*Ur@Su6q(KpZcfp18/XG');
define('SECURE_AUTH_KEY',  'm8Nk)C4%}wV$:=CS?Qj<^1efU*9`pSjB+rN+3/[Fj|i==Rv:(41Ry-1$ECnHs+Nc');
define('LOGGED_IN_KEY',    '.hjNWA|X|Nt-TyMw}+E{4nmlb>gfS.0|{+_s)l=k|VaV/dTOUr}we}BqGpu|oaWT');
define('NONCE_KEY',        '51{K3jGGxR>ua{449ZT-uarb89uz#|jNd]fx:{8]_|Kg{d-%VNo}IpuG?|_%t@q$');
define('AUTH_SALT',        'A@: .=FUW<|%.4?h9jq92 U:Bx`sYP@8-q 5{43-eOoaoq,T28 +.eUwZ|p+hn<a');
define('SECURE_AUTH_SALT', '6IHS`Tfs8;Mc&+bX-xP`x:=d5Oxad^y4{POa+FfOUD1s=mTw>9g}:`I7* ,sD3k,');
define('LOGGED_IN_SALT',   'WT@3aKo@{S*7$/@$B](+$[%()>!NaF}.FV+^Uw{F7=oF.PUa@g[:Qgs(]jZ};8H+');
define('NONCE_SALT',       '}X||sE%dMhxjBr:=LvTprR|vwWwm?vvT=s[]|H/^/>6[*Cyz?,#69}V>jfO0]SXc');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'emp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
