<ul><?php pll_the_languages(
        $defaults = array(
			'dropdown'               => 0, // display as list and not as dropdown
			'echo'                   => 1, // echoes the list
			'hide_if_empty'          => 1, // hides languages with no posts (or pages)
			'menu'                   => 0, // not for nav menu
			'show_flags'             => 1, // don't show flags
			'show_names'             => 0, // show language names
			'display_names_as'       => 'name', // valid options are slug and name
			'force_home'             => 0, // tries to find a translation
			'hide_if_no_translation' => 0, // don't hide the link if there is no translation
			'hide_current'           => 1, // don't hide current language
			'post_id'                => null, // if not null, link to translations of post defined by post_id
		)
);?></ul>
