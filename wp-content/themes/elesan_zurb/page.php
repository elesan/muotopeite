<?php
/**
 * Page
 *
 * Loop container for page content
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 4.0
 */

get_header('elesan'); ?>

<div id="totop"></div>

<div style="background-color: #f4f4f4; width: 100%">
    
<!-- header -->
<div class="bar"></div>

<div class="show-for-small hide-for-medium hide-for-medium-up hide-for-large-up">
    
    <div class="left bottommargin">
        
        <div class="show-for-portrait">
            <div class="toplogo">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" />
            </div>
            <div class="topmargin">                
                <a href="#" data-reveal-id="pick-lang" class="tiny button secondary"><img src="<?php echo get_template_directory_uri(); ?>/images/langs.png" alt="Languages" /></a>
            </div>
        </div>
        
        <div class="show-for-landscape">
            <div class="toplogo">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" />
            </div>
            <div class="topmargin">
                <a href="#" data-reveal-id="pick-lang" class="tiny button secondary"><img src="<?php echo get_template_directory_uri(); ?>/images/langs.png" alt="Languages" /></a>
            </div>
        </div>
        
    </div>
    
    <div class="right">
        
        <div class="show-for-portrait">
            <nav id="nav-menu-small-portrait" class="show-for-small hide-for-medium hide-for-medium-up hide-for-large-up">
            <?php wp_nav_menu( 
                    array( 'theme_location' => 'header-menu',
                        'menu_class' => 'left',
                        'container' => '',
                        'fallback_cb' => 'foundation_page_menu',
                        'walker' => new elesan_navigation() ) ); ?>
            </nav>
        </div>
        
        <div class="show-for-landscape">
            <nav id="nav-menu-small-landscape" class="show-for-small hide-for-medium hide-for-medium-up hide-for-large-up">
            <?php wp_nav_menu( 
                    array( 'theme_location' => 'header-menu',
                        'menu_class' => 'left',
                        'container' => '',
                        'fallback_cb' => 'foundation_page_menu',
                        'walker' => new elesan_navigation() ) ); ?>
            </nav>
        </div>
        
    </div>

    <div class="row">
        <div class="small-12 columns">
            <h5><?php echo __('We cover everything, literally!', THEME_NAME);?></h5>
            <p class="smallertext"><?php echo __('Producers of PVC covers master all stages of cover production - from the first contact with the client to product maintenance. All our products are hand-made, making your product unique and tailored to your specific needs. We offer full repair and maintenance service for all our covers. Our extensive spare part stock can help you out if your cover needs immediate repair. We are happy to serve you in six languages (Estonian, Russian, Finnish, Swedish, English, and German).<br />Contact us!', THEME_NAME);?>
            </p>
        </div>
    </div>

</div>

<div class="show-for-medium hide-for-medium-down hide-for-large hide-for-xlarge">
    
    <div class="row">
        <div class="12-large columns">
    
            <div class="left show-for-medium hide-for-medium-down hide-for-large hide-for-xlarge">
                <div class="topmargin">
                    <a href="#" data-reveal-id="pick-lang" class="tiny button secondary"><img src="<?php echo get_template_directory_uri(); ?>/images/langs.png" alt="Languages" /></a>
                </div>
            </div>

            <div class="right">
                <div style="height: 130px;">
                <nav id="nav-menu-medium" class="show-for-medium hide-for-medium-down hide-for-large hide-for-xlarge">
                <?php wp_nav_menu( 
                        array( 'theme_location' => 'header-menu',
                            'menu_class' => 'left',
                            'container' => '',
                            'fallback_cb' => 'foundation_page_menu',
                            'walker' => new elesan_navigation() ) ); ?>
                </nav>
                </div>
            </div>
            
            <div class="row">
                <div class="large-12 columns">
                    <div class="row">
                        <div class="large-6 columns">
                            <div class="toplogo">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" />
                            </div>
                        </div>
                        <div class="large-6 columns">
                            <h5><?php echo __('We cover everything, literally!', THEME_NAME);?></h5>
                            <p class="smallertext"><?php echo __('Producers of PVC covers master all stages of cover production - from the first contact with the client to product maintenance. All our products are hand-made, making your product unique and tailored to your specific needs. We offer full repair and maintenance service for all our covers. Our extensive spare part stock can help you out if your cover needs immediate repair. We are happy to serve you in six languages (Estonian, Russian, Finnish, Swedish, English, and German).<br />Contact us!', THEME_NAME);?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
</div>

<div class="show-for-large-up">
    
    <div class="row">
        <div class="12-large columns">
    
            <div class="left show-for-large-up">
                <div class="topmargin">
                    <a href="#" data-reveal-id="pick-lang" class="tiny button secondary"><img src="<?php echo get_template_directory_uri(); ?>/images/langs.png" alt="Languages" /></a>
                </div>
            </div>

            <div class="right" style="height: 130px;">
                <nav id="nav-menu-large" class="show-for-large-up">
                <?php wp_nav_menu( 
                        array( 'theme_location' => 'header-menu',
                            'menu_class' => 'left',
                            'container' => '',
                            'fallback_cb' => 'foundation_page_menu',
                            'walker' => new elesan_navigation() ) ); ?>
                </nav>
                
            </div>
            
            <div class="row">
                <div class="large-12 columns">
                    <div class="row">
                        <div class="large-6 columns">
                            <div class="toplogo">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" />
                            </div>
                        </div>
                        <div class="large-6 columns">
                            <h5><?php echo __('We cover everything, literally!', THEME_NAME);?></h5>
                            <p class="smallertext"><?php echo __('Producers of PVC covers master all stages of cover production - from the first contact with the client to product maintenance. All our products are hand-made, making your product unique and tailored to your specific needs. We offer full repair and maintenance service for all our covers. Our extensive spare part stock can help you out if your cover needs immediate repair. We are happy to serve you in six languages (Estonian, Russian, Finnish, Swedish, English, and German).<br />Contact us!', THEME_NAME);?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
</div>
    
<!-- end header -->

<div class="barbottom"></div>
</div>

<!-- Main Content -->
    
<div class="row">
    <div class="large-12 columns topmargin bottommargin" role="main">

        <?php if ( have_posts() ) : ?>

                <?php while ( have_posts() ) : the_post(); ?>
                        <?php get_template_part( 'content', 'page' ); ?>
                <?php endwhile; ?>

        <?php endif; ?>
        
    </div>
</div>
    
<div class="row">
    <div class="large-12 columns">
        <hr/>
    </div>
</div>
    
<div class="row">
    <div class="large-12 columns topmargin">
        <div class="flex-video widescreen vimeo">
            <iframe width="950" height="500" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps/ms?msa=0&amp;msid=206631261975551947261.0004f026bae0e2ce4adca&amp;ie=UTF8&amp;t=m&amp;z=17&amp;output=embed"></iframe>
        </div>
    </div>
</div>
    
<!-- End Main Content -->

<?php get_footer('elesan')?>