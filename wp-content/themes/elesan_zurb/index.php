<?php
/**
 * Index
 *
 * Standard loop for the front-page
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 4.0
 */
    $current_language = pll_current_language();
    $content = emp_get_categories_in_english($current_language);
    $paths=wp_upload_dir();
    get_header('elesan'); 
    $slides = emp_getSLIDES();
    $title = 'title_'.$current_language; 
    $text = 'text_'.$current_language;
?>

<div id="totop"></div>

<div id="mask">
    <div style="padding-top: 30%;">
        <img class="load" src="<?php echo get_template_directory_uri(); ?>/images/loading.gif" alt="Loading..." /><br />
        <img class="load" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" width="220" />
    </div>
</div>

<div style="background-color: #f4f4f4; width: 100%">
    
<!-- header -->
<div class="bar"></div>

<div class="show-for-small hide-for-medium hide-for-medium-up hide-for-large-up">
    
    <div class="left bottommargin">
        
        <div class="show-for-portrait">
            <div class="toplogo">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" />
            </div>
            <div class="topmargin">                
                <a href="#" data-reveal-id="pick-lang" class="tiny button secondary"><img src="<?php echo get_template_directory_uri(); ?>/images/langs.png" alt="Languages" /></a>
            </div>
        </div>
        
        <div class="show-for-landscape">
            <div class="toplogo">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" />
            </div>
            <div class="topmargin">
                <a href="#" data-reveal-id="pick-lang" class="tiny button secondary"><img src="<?php echo get_template_directory_uri(); ?>/images/langs.png" alt="Languages" /></a>
            </div>
        </div>
        
    </div>
    
    <div class="right">
        
        <div class="show-for-portrait">
            <nav id="nav-menu-small-portrait" class="show-for-small hide-for-medium hide-for-medium-up hide-for-large-up">
            <?php wp_nav_menu( 
                    array( 'theme_location' => 'header-menu',
                        'menu_class' => 'left',
                        'container' => '',
                        'fallback_cb' => 'foundation_page_menu',
                        'walker' => new elesan_navigation() ) ); ?>
            </nav>
        </div>
        
        <div class="show-for-landscape">
            <nav id="nav-menu-small-landscape" class="show-for-small hide-for-medium hide-for-medium-up hide-for-large-up">
            <?php wp_nav_menu( 
                    array( 'theme_location' => 'header-menu',
                        'menu_class' => 'left',
                        'container' => '',
                        'fallback_cb' => 'foundation_page_menu',
                        'walker' => new elesan_navigation() ) ); ?>
            </nav>
        </div>
        
    </div>

    <div class="row">
        <div class="small-12 columns">
            <h5><?php echo __('We cover everything, literally!', THEME_NAME);?></h5>
            <p class="smallertext"><?php echo __('Producers of PVC covers master all stages of cover production - from the first contact with the client to product maintenance. All our products are hand-made, making your product unique and tailored to your specific needs. We offer full repair and maintenance service for all our covers. Our extensive spare part stock can help you out if your cover needs immediate repair. We are happy to serve you in six languages (Estonian, Russian, Finnish, Swedish, English, and German).<br />Contact us!', THEME_NAME);?>
            </p>
        </div>
    </div>

</div>

<div class="show-for-medium hide-for-medium-down hide-for-large hide-for-xlarge">
    
    <div class="row">
        <div class="12-large columns">
    
            <div class="left show-for-medium hide-for-medium-down hide-for-large hide-for-xlarge">
                <div class="topmargin">
                    <a href="#" data-reveal-id="pick-lang" class="tiny button secondary"><img src="<?php echo get_template_directory_uri(); ?>/images/langs.png" alt="Languages" /></a>
                </div>
            </div>

            <div class="right">
                <div style="height: 130px;">
                <nav id="nav-menu-medium" class="show-for-medium hide-for-medium-down hide-for-large hide-for-xlarge">
                <?php wp_nav_menu( 
                        array( 'theme_location' => 'header-menu',
                            'menu_class' => 'left',
                            'container' => '',
                            'fallback_cb' => 'foundation_page_menu',
                            'walker' => new elesan_navigation() ) ); ?>
                </nav>
                </div>
            </div>
            
            <div class="row">
                <div class="large-12 columns">
                    <div class="row">
                        <div class="large-5 columns">
                            <div class="toplogo">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" />
                            </div>
                        </div>
                        <div class="large-7 columns" style="padding-left: 5.06em;">
                            <h5><?php echo __('We cover everything, literally!', THEME_NAME);?></h5>
                            <p class="smallertext"><?php echo __('Producers of PVC covers master all stages of cover production - from the first contact with the client to product maintenance. All our products are hand-made, making your product unique and tailored to your specific needs. We offer full repair and maintenance service for all our covers. Our extensive spare part stock can help you out if your cover needs immediate repair. We are happy to serve you in six languages (Estonian, Russian, Finnish, Swedish, English, and German).<br />Contact us!', THEME_NAME);?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
</div>

<div class="show-for-large-up">
    
    <div class="row">
        <div class="12-large columns">
    
            <div class="left show-for-large-up">
                <div class="topmargin">
                    <a href="#" data-reveal-id="pick-lang" class="tiny button secondary"><img src="<?php echo get_template_directory_uri(); ?>/images/langs.png" alt="Languages" /></a>
                </div>
            </div>

            <div class="right" style="height: 130px;">
                <nav id="nav-menu-large" class="show-for-large-up">
                <?php wp_nav_menu( 
                        array( 'theme_location' => 'header-menu',
                            'menu_class' => 'left',
                            'container' => '',
                            'fallback_cb' => 'foundation_page_menu',
                            'walker' => new elesan_navigation() ) ); ?>
                </nav>
                
            </div>
            
            <div class="row">
                <div class="large-12 columns">
                    <div class="row">
                        <div class="large-5 columns">
                            <div class="toplogo">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" />
                            </div>
                        </div>
                        <div class="large-7 columns" style="padding-left: 5.1em;">
                            <h5><?php echo __('We cover everything, literally!', THEME_NAME);?></h5>
                            <p class="smallertext"><?php echo __('Producers of PVC covers master all stages of cover production - from the first contact with the client to product maintenance. All our products are hand-made, making your product unique and tailored to your specific needs. We offer full repair and maintenance service for all our covers. Our extensive spare part stock can help you out if your cover needs immediate repair. We are happy to serve you in six languages (Estonian, Russian, Finnish, Swedish, English, and German).<br />Contact us!', THEME_NAME);?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    
</div>
    
<!-- end header -->

<div class="row">
    <div class="large-12 columns hide-for-small">
        <hr />
    </div>
</div>


<!-- slider -->
    
    <div class="show-for-small hide-for-medium hide-for-medium-up hide-for-large-up clear">

        <div class="show-for-landscape">
            <?php if(count($slides)):?>
                <div class="row">
                    <div class="large-12 columns" role="main">
                        <div class="slideshow-wrapper">
                        <ul data-orbit> 
                        <?php $i=0; foreach ($slides as $slide): $i++?>
                        <li>
                            <div class="row">
                                <div class="small-12 columns">
                                    <div class="row">
                                        <div class="small-6 columns">
                                            <h5 class="sliderheader"><?php echo $slide->$title?></h5>
                                            <p class="smallertext"><?php echo $slide->$text?></p>
                                            <div class="sliderbutton">
                                                <a href="#" id="subtitle4-id<?php echo $i; $i++?>-<?php echo $slide->cat_ID; ?>" class="small button secondary change-content subtitles"><?php echo __('Read more...', THEME_NAME);?></a>
                                            </div>
                                        </div>
                                        <div class="small-6 columns">
                                            <img src="<?php echo $paths['baseurl'].'/' . $slide->name?>" class="slideright" alt="Sliderimg" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <?php endforeach;?>
                        </ul>
                        </div>
                    </div>
                </div>
        </div>

        <div class="show-for-portrait">
                <div class="row">
                    <div class="large-12 columns" role="main">
                        <div class="slideshow-wrapper">
                        <ul data-orbit> 
                            <?php  foreach ($slides as $slide):?>
                                <li>
                                    <img src="<?php echo $paths['baseurl'].'/' . $slide->name?>" alt="Sliderimg" />

                                    <h5 class="sliderheader"><?php echo $slide->$title?></h5>
                                    <p class="smallertext"><?php echo $slide->$text?></p>
                                    <div class="sliderbutton">
                                        <a href="#" id="subtitle3-id<?php echo $i; $i++?>-<?php echo $slide->cat_ID; ?>" class="small button secondary change-content subtitles"><?php echo __('Read more...', THEME_NAME);?></a>
                                    
                                    </div>

                                </li>
                            <?php endforeach;?>
                        </ul>
                        </div>
                    </div>
                </div>
        </div>


            <?php endif;?>
    </div>

    <div class="show-for-medium hide-for-medium-down hide-for-large hide-for-xlarge">

        <?php if(count($slides)):?>
            <div class="row">
                <div class="large-12 columns" role="main">
                    <div class="slideshow-wrapper">
                    <ul data-orbit> 
                    <?php foreach ($slides as $slide):?>
                        <li>
                            <div class="row">
                                <div class="small-12 columns">
                                    <div class="row">
                                        <div class="small-5 columns slideleft">
                                            <h5 class="sliderheader"><?php echo $slide->$title?></h5>
                                            <p class="smallertext"><?php echo $slide->$text?></p>
                                        </div>
                                        <div class="small-7 columns">
                                            <img src="<?php echo $paths['baseurl'].'/' . $slide->name?>" class="slideright" alt="Sliderimg" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach;?>
                    </ul>
                    </div>
                </div>
            </div>
        <?php endif;?>

    </div>

    <div class="show-for-large-up">

        
        <?php if(count($slides)):?>
            <div class="row">
                <div class="large-12 columns" role="main">
                    <div class="slideshow-wrapper">
                    <ul data-orbit> 
                    <?php foreach ($slides as $slide): ?>
                        <li>
                            <div class="row">
                                <div class="large-12 columns">
                                    <div class="row">
                                        <div class="large-5 columns slideleft">
                                            <h5 class="sliderheader"><?php echo $slide->$title?></h5>
                                            <p class="smallertext"><?php echo $slide->$text?></p>
                                            <div class="sliderbutton">
                                                <a href="#" id="subtitle1-id<?php echo $i; $i++?>-<?php echo pll_get_term($slide->cat_ID, $current_language);?>" class="small button secondary change-content-readmore subtitles"><?php echo __('Read more...', THEME_NAME);?></a>
                                            </div>
                                        </div>

                                        <div class="large-7 columns">
                                            <img src="<?php echo $paths['baseurl'].'/' . $slide->name?>" class="slideright" alt="Sliderimg" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endforeach;?>
                    </ul>
                    </div>
                </div>
            </div>
        <?php endif;?>

    </div>
    
    <div class="barbottom"></div>
    
<!-- end slider -->

</div>

<!-- content -->

<!--<div class="row">
    <div class="small-6 columns topmargin">
        <table style="border-collapse: collapsed; border: none;">
            <tr>
                <td style="height: 1.5em; width: 5em; background-color: #369abe;"></td>
                <td></td>
                <td style="height: 1.5em; width: 14em; background-color: #46cbb0; margin-left: 4px; color: #fff; font-size: 1em; font-weight: bold; text-transform: uppercase;">Tooted</td>
            </tr>
        </table>
    </div>
</div>-->

<div class="row">
    <div class="large-12 columns bottommargin topmargin categories" role="main">
        <div id="subtitles">
            
         <dl class="sub-nav">
             <dd>
                 <div style="height: 1.57em; padding-left: 5px; padding-right: 5px; background-color: #504f53; color: #fff; text-transform: uppercase; font-weight: bold;">
                     <?php echo __('Products:', THEME_NAME);?>
                 </div>
             </dd>
         <?php $f = true; $i=0; $u=1; foreach ($content as $s): if($f) $cat_id = $s->category->cat_ID; $f = false;?>
	    <dd style="margin-left: 0.28em;" id="sub-list-item-<?php echo $s->category->cat_ID?>" 
                <?php 
                echo 'class="sub-list-items';
                if($u==1) echo ' active'; $u++;
                echo '"';?>>
                <div style="color: #aaa"><a href="#" id="subtitle-id-<?php echo $s->category->cat_ID; ?>" class="change-content subtitles">
	    <?php echo $s->category->name?></a>&nbsp;&nbsp;/</div>

        <?php endforeach; ?>
	</dd>
         </dl>

        </div>
    </div>
</div>

<div class="row">
    <div class="large-12 columns bottommargin" id="category-content">  
    <?php $text_content = emp_get_posts_by_cat($cat_id);
      $images = getCategoryPicturesByIdNOSLIDE(pll_get_term($cat_id, 'en'))?>

        <div class="bottommargin">
            <?php 
            $object_single_post = get_post($text_content[0]->ID);
            $content = apply_filters ("the_content", $object_single_post->post_content);?>
        <h4><?php echo $text_content[0]->post_title?></h4>
        <?php echo $content?>
        </div>
        <?php if(count($images)):?>
        <ul class="clearing-thumbs" data-clearing>
            <?php foreach ($images as $cimg):?>
            <li class="article-thumbs">
                <a href="<?php echo $paths['baseurl'].'/' . $cimg->name?>" class="th">
                    <img data-caption="<?php echo $cimg->$title?>" src="<?php 
                    echo $paths['baseurl'].'/th_' . $cimg->name?>" class="content-img" alt="Thumbnail" />
                </a>
            </li>
            <?php endforeach; ?>
        </ul>
        <?php endif;?>
    </div>
</div>

<!-- end content -->

<div id="loading">
    <img class="load" src="<?php echo get_template_directory_uri(); ?>/images/loading.gif" alt="Loading..." /><br />
    <img class="load" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Eesti Muotopeite" width="220" />
</div>

<?php get_footer('elesan');
