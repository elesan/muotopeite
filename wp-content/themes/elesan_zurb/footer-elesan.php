<?php
/**
 * Footer
 *
 * Displays content shown in the footer section
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 4.0
 */
?>

<footer>
    <div class="barbottom"></div>
    <div class="footerbg">
        
        <div class="show-for-small hide-for-medium hide-for-medium-up hide-for-large-up clear">

            <div class="show-for-portrait">
                <div class="row">
                    <div class="small-12 columns">
                        <div class="panel topmargin">
                            <p class="smallertext">
                            <b>Muotopeite Eesti OÜ</b><br/>
                            </p>
                            <p class="smallertext" style="font-weight: bold;">
                            <script type="text/javascript">showEmailLink_small("info","muotopeite.ee","");</script><br/>
                            <a href="tel:%20+372%20502%209056">+372 502 9056</a>
                            </p>
                            <p class="smallertext" style="font-weight: bold;">
                            Kaubasadama tee 18, 88317, Audru vald, Pärnumaa, Mauri Tehnoküla, Maja 1 Box 23-24<br/>
                            Pärnumaa, Eesti
                            </p>
                            <p class="smallertext">
                                <b><a href="http://www.muotopeite.ee/">www.muotopeite.ee</a></b>
                            </p>

                            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="footer logo" style="width: 8em;" />
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="show-for-landscape">
                <div class="row">
                    <div class="small-12 columns">
                        <div class="panel topmargin">
                            <div class="row">
                                <div class="small-6 columns">
                                    <p class="smallertext">
                                    <b>Muotopeite Eesti OÜ</b><br/>
                                    </p>
                                    <p class="smallertext">
                                        <b><a href="http://www.muotopeite.ee/">www.muotopeite.ee</a></b>
                                    </p>
                                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="footer logo" style="width: 8em;" />
                                </div>
                                <div class="small-6 columns">
                                    <p class="smallertext" style="font-weight: bold;">
                                    <script type="text/javascript">showEmailLink_small("info","muotopeite.ee","");</script><br/>
                                    <a href="tel:%20+372%20502%209056">+372 502 9056</a>
                                    </p>
                                    <p class="smallertext" style="font-weight: bold;">
                                    Kaubasadama tee 18, 88317, Audru vald, Pärnumaa, Mauri Tehnoküla, Maja 1 Box 23-24<br/>
                            Pärnumaa, Eesti
                                    </p>
             
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        <a href="#" class="button secondary backtotop totopbutton"><i class="general foundicon-up-arrow"></i></a>
        <a href="#" data-reveal-id="contactModal" class="button mailus"><i class="general foundicon-mail"></i></a>
        <a href="tel:%20+372%20502%209056" class="button phoneus"><i class="general foundicon-phone"></i></a>
            
        </div>

        <div class="show-for-medium-up">
            
            <div class="row">
                <div class="small-12 columns topmargin">
                    <ul class="breadcrumbs" style="font-weight: bold;">
                        <li>
                            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="footer logo" style="width: 8em;" />
                        </li>
                        <li class="current" style="text-transform: none;">
                            Kaubasadama tee 18, 88317, Audru vald, Pärnumaa, Mauri Tehnoküla, Maja 1 Box 23-24, Pärnumaa, Eesti
                        </li>
                        <li class="current">
                            <script type="text/javascript">showEmailLink_small("info","muotopeite.ee","");</script>
                        </li>
                        <li class="current">
                            <a href="tel:%20+372%20502%209056" style="cursor: pointer;">+372 502 9056</a>
                        </li>
                        <li class="current" style="text-transform: none;">
                            Muotopeite Eesti OÜ
                        </li>
          
                        <li>
                            <a href="#" data-reveal-id="contactModal"><?php echo __('Contact Us!', THEME_NAME);?></a>
                        </li>
                    </ul>
                </div>
            </div>
            
        </div>

    </div>
</footer>
<!-- End Footer -->
<?php wp_footer(); ?>

<div class="hide-for-small hide-for-large hide-for-xlarge">

    <a href="#" class="button secondary backtotop totopbutton-large"><i class="general foundicon-up-arrow"></i></a>
    <a href="#" data-reveal-id="contactModal" class="button mailus-large"><i class="general foundicon-mail"></i></a>

</div>

<div id="pick-lang" class="reveal-modal small">
    <ul class="langlist inline-list"><?php pll_the_languages(
                            $defaults = array(
                                            'dropdown'               => 0, // display as list and not as dropdown
                                            'echo'                   => 1, // echoes the list
                                            'hide_if_empty'          => 1, // hides languages with no posts (or pages)
                                            'menu'                   => 0, // not for nav menu
                                            'show_flags'             => 1, // don't show flags
                                            'show_names'             => 1, // show language names
                                            'display_names_as'       => 'name', // valid options are slug and name
                                            'force_home'             => 0, // tries to find a translation
                                            'hide_if_no_translation' => 0, // don't hide the link if there is no translation
                                            'hide_current'           => 1, // don't hide current language
                                            'post_id'                => null, // if not null, link to translations of post defined by post_id
                                    )
    )?></ul>
    <a class="close-reveal-modal" style="font-size: 2em;">&#215;</a>
</div>

<div id="contactModal" class="reveal-modal center medium">
    <div class="show-for-all">
        <form id="emailForm" method="post" action="http://muotopeite.ee/wp-content/themes/elesan_zurb/email-send.php">
            <fieldset>
            <legend><?php echo __('Contact Us!', THEME_NAME); ?></legend>
            <div class="large-12 columns">
                <div class="hide-for-large hide-for-large-up hide-for-xlarge">
                <script type="text/javascript">showEmailLink_icon("info","muotopeite.ee","");</script>
                </div>
                <span class="message-area"></span>
            </div>
                
            <div class="large-12 columns">
            <input type="hidden" name="confirm" value="set" />
            <input type="text" name="name" class="required" placeholder="<?php echo __('Name', THEME_NAME); ?>" />
            </div>
            
            <div class="large-12 columns">
            <input type="text" name="email" class="required email" placeholder="<?php echo __('E-mail', THEME_NAME); ?>" />
            </div>
            
            <div class="large-12 columns">
            <input type="text" name="subject" class="required" placeholder="<?php echo __('Title', THEME_NAME); ?>" />
            </div>
            
            <div class="large-12 columns">
            <textarea name="content" rows="10" class="required" style="height: 8em;" placeholder="<?php echo __('Message', THEME_NAME); ?>"></textarea>
            </div>
            
            <div class="large-12 columns">
            <button class="submit-button" style="width: 100%;"><?php echo __('Send direct message', THEME_NAME); ?></button>
            </div>
                
            </fieldset>
        </form>
        
        <div class="topmargin">
            <ul class="breadcrumbs" style="font-weight: bold; width: 100%;">
                <li>
                    <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="footer logo" style="width: 8em;" />
                </li>
                <li class="current" style="text-transform: none;">
                    Kaubasadama tee 18, 88317, Audru vald, Pärnumaa, Mauri Tehnoküla, Maja 1 Box 23-24, Pärnumaa, Eesti
                </li>
                <li class="current">
                    <script type="text/javascript">showEmailLink_small("info","muotopeite.ee","");</script>
                </li>
                <li class="current">
                    <a href="tel:%20+372%20502%209056" style="cursor: pointer;">+372 502 9056</a>
                </li>
                <li class="current" style="text-transform: none;">
                    Muotopeite Eesti OÜ
                </li>
            </ul>
        </div>
        
    </div>
  <a class="close-reveal-modal" style="font-size: 2em;">&#215;</a>
</div>
<?php if(isset($_GET['cid'])): $cid = (int) $_GET['cid']; if($cid>=0 && $cid<=200): ?>
    <script type="text/javascript">
        var sel = "#subtitle-id-"+<?php echo $cid?>;
        if ( $(sel).length ) $(sel).trigger('click');
    </script>
<?php  endif; endif;
?>
</body>
</html>
