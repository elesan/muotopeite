<?php
/**
 * Header
 *
 * Setup the header for our theme
 *
 * @package WordPress
 * @subpackage Foundation, for WordPress
 * @since Foundation, for WordPress 4.0
 */
?>

<!DOCTYPE html>
<!--[if IE 8]> 				 <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?>> <!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" />
<link rel="apple-touch-icon-precomposed" sizes="114×114" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-114×114-precomposed.png" />
<link rel="apple-touch-icon-precomposed" sizes="72×72" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon-72×72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/images/touch-icon-iphone-precomposed.png">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- Set the viewport width to device width for mobile -->
<meta name="viewport" content="width=device-width" />

 <title><?php bloginfo('name'); ?></title>

<?php wp_head()?>

<script type="text/javascript">
    function showEmailLink(user, domain, linkText) {
        if (linkText == '') {
            linkText = user + '@' + domain;
        }
        return document.write('<a href=' + 'mail' + 'to:' + user + '@' + domain + ' class="button" style="width: 100%;" >' + 'info' + '@' + 'muotopeite.ee' + '<\/a>');
    }
    
    function showEmailLink_small(user, domain, linkText) {
        if (linkText == "") {
            linkText = user + "@" + domain;
        }
        return document.write("<a href=" + "mail" + "to:" + user + "@" + domain + ' style="cursor: pointer; text-transform: none;" >' + linkText + "<\/a>");
    }
    
    function showEmailLink_icon(user, domain, linkText) {
        if (linkText == "") {
            linkText = user + "@" + domain;
        }
        return document.write(  '<ul class="button-group" style="width: 100%">' +
                                    '<li style="width: 50%"><a href="tel:%20+372%20502%209056" class="button" style="width: 100%"><i class="general foundicon-phone"><\/i><\/a></li>' +
                                    '<li style="width: 50%"><a href="' + 'mail' + 'to:' + user + "@" + domain + '" class="button" style="width: 100%"><i class="general foundicon-mail"></i><\/a><\/li>' +
                                '<\/ul>');
        //return document.write("<a href=" + "mail" + "to:" + user + "@" + domain + ' class="button" style="width: 50%; display: inline-block;" ><i class="general foundicon-mail"></i><\/a>');
    }
</script>

<script>
var LANG = "<?php $current_language = pll_current_language(); echo $current_language?>";
</script>

<style type="text/css">
    @font-face {
        font-family: 'general';
        src: url('<?php echo get_template_directory_uri(); ?>/foundation_icons_general/fonts/general_foundicons.eot');
        src: url('<?php echo get_template_directory_uri(); ?>/foundation_icons_general/fonts/general_foundicons.eot?#iefix') format('embedded-opentype'),
             url('<?php echo get_template_directory_uri(); ?>/foundation_icons_general/fonts/general_foundicons.woff') format('woff'),
             url('<?php echo get_template_directory_uri(); ?>/foundation_icons_general/fonts/general_foundicons.ttf') format('truetype'),
             url('<?php echo get_template_directory_uri(); ?>/foundation_icons_general/fonts/general_foundicons.svg#GeneralFoundicons') format('svg');
        font-weight: normal;
        font-style: normal;
    }
</style>

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/foundation_icons_general1/stylesheets/general_foundicons.css" />
<!--[if lt IE 8]><link rel="stylesheet" href="<?php echo get_template_directory_uri(); 

?>/foundation_icons_general/stylesheets/accessibility_foundicons_ie7.css"><![endif]-->
<meta name="google-site-verification" content="ffauUOoBIAVANpQ1U6rRs8zNSZPleykDPL13rolkZ6o" />
</head>
