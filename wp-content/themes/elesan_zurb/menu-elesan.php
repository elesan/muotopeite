
<!--
<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo('name'); ?></a></h1>
-->
<nav id="nav-menu-large">
    <?php wp_nav_menu( 
            array( 'theme_location' => 'header-menu',
                'menu_class' => 'left',
                'container' => '',
                'fallback_cb' => 'foundation_page_menu',
                'walker' => new elesan_navigation() ) ); ?>
</nav>
