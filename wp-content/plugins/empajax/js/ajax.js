/* 
 * @authors www.elesan.ee Sander, Daniel, Henri
 */
var global_clean_text = true;
function removeSpace(mg)
{
    if(mg === null || mg === '' || mg === ' ' || (typeof mg != "string")){
        throw "incorrect input";
    }
    var n = mg.split(" ");
    if(n.length === 1) {return n[0];}
    var r="";
    r = n.join("&nbsp;");
    return r;
}

function cleanRotate(){
    if(global_clean_text === true){
        var target_links =  $('.rotate');
        $.each( target_links, function( key, value ) {
          var old_text = $(this).text();
          $(this).html(removeSpace(old_text));
        });
        global_clean_text = false;
    }
    $(".rotate").rotate(rot_deg);
}
var rot_deg = 90;
var breakpoint = 767;
function topBarCSSLarge(){
    $(".rotate").rotate(0);
}

function topBarCSSSmall(){
    cleanRotate();
}

if($(window).width() > breakpoint){
    $(".rotate").rotate(0);
    //$("#nav-menu-large").show();
}
else{
    topBarCSSSmall();
}
if($(window).width() > 939){
    menuEffect();
}
$(window).resize(function() {
    if($(window).width() > breakpoint){
        $(".rotate").rotate(0);
        
    } 
    else{
        topBarCSSSmall();
    }
});
    
jQuery(document).foundation();

function getLastEl(el){
    var a = el.split("-");
    a.reverse();
    return a[0];
}

/**
 * 
 * @param #id form
 * @returns {boolean}
 */
function validateForm(form){
    
    var button = form.find(".submit-button");
    var required = form.find(".required");
    var email = form.find(".email");
    
    //Required checking
    required.blur(function() {
        requiredCheck($(this));
    });
    
    //Email checking
    email.blur(function() {
        requiredCheck($(this));
        emailCheck($(this));
    });

    //Form checking
    button.click(function(event) {
        event.preventDefault();
        
        //Submit ajax request if form is valid
        if(validator(form)){
            
            var serializedData = form.serialize();
            var request = $.ajax({
                url : form.attr('action'),
                type: form.attr('method'),
                data: serializedData
            });

            // callback handler that will be called on success
            request.done(function (response, textStatus, jqXHR){
                form.find('.submit-button').prop("disabled", true).addClass('success');
                
            });

            // callback handler that will be called on failure
            request.fail(function (jqXHR, textStatus, errorThrown){
                form.find('.submit-button').prop("disabled", true).addClass('error');
            });

            // callback handler that will be called regardless
            // if the request failed or succeeded
            request.always(function () {
                // reenable the inputs
                form.find('.submit-button').prop("disabled", true);
            });
        }
        //Else- invalid form
        else{
            
        }
    });
    
    return false;
}

function submitForm(form){
    $.ajax({
        type     : "POST",
        cache    : false,
        url      : form.attr('action'),
        data     : form.serializeArray(),
    });
}

/**
 * 
 * @param #id form
 * @returns {Boolean}
 */
function validator(form){
    
    var requiredElements = form.find(".required");
    var emailElements = form.find(".email");
    
    var messageContainer = $(".message-container");


    var isValid = true;

    // Check required inputs
    $.each(requiredElements, function( value ) {
        //Error, element not valid
        if( !requiredCheck($(this)) ){
            isValid = false;
        }

    });

    //Check e-mail inputs
    $.each(emailElements, function( value ) {
        //Error, input not valid
        if( !emailCheck($(this)) ){
            isValid = false;
        }
    });

    //Form validation by elements
    if(isValid){ 
        return true;
    }

    else{
        return false;
    }
}

/**
 * 
 * @param .class element
 * @returns {Boolean}
 */
function requiredCheck(element){
    if(element.val()){
        element.removeClass('error');
        element.addClass('success');
        return true;
    }
    else{
        element.removeClass('success');
        element.addClass('error');
        return false;
    }
}

/**
 * 
 * @param .class element
 * @returns {Boolean}
 */
function emailCheck(element){
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    
    if(regex.test(element.val())) {
        element.removeClass('error');
        element.addClass('success');
        return true;
    }
    
    else{
        element.removeClass('success');
        element.addClass('error');
        return false;
    }
}
//http://stackoverflow.com/questions/3432656/scroll-to-a-div-using-jquery


var time;
var time1;
var time_ajax;
function clearTimer(){
    clearTimeout(time);
}
function maskTimer(){
    time=setTimeout(function(){$('#mask').fadeOut(1000); clearTimer();}, 1000);
}

function changeActiveClass(id) {
    $('.sub-list-items').each(function() {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        }
    });
    $( '#sub-list-item-' + id.toString() ).addClass('active');
}

$(".change-content").click(function(event) {
    event.preventDefault();
    document.getElementById("category-content").innerHTML = '';
    document.getElementById("category-content").innerHTML = $("#loading").html();   
    var id = getLastEl(this.id);
    var ajax = jQuery.ajax({
        type: "POST",
        url : MyAjax.ajaxurl,
        data : {
                action : 'myajax-submit',
                id : id,
                lang : LANG
        },
        dataType: "html",
        cache: false
    });
    ajax.done(function(data) {
        time_ajax=setTimeout(function(){
                clearTimeout(time_ajax);
                $("#category-content").html(data);
                changeActiveClass(id);
                $('html, body').animate({ scrollTop: $('#category-content').offset().top }, 'slow');
            }, 500);

    });
});

$(".change-content-readmore").click(function(event) {
    event.preventDefault();
    document.getElementById("category-content").innerHTML = '';
    document.getElementById("category-content").innerHTML = $("#loading").html();   
    var id = getLastEl(this.id);
    var ajax = jQuery.ajax({
        type: "POST",
        url : MyAjax.ajaxurl,
        data : {
                action : 'myajax-submit',
                id : id,
                lang : LANG
        },
        dataType: "html",
        cache: false
    });
    ajax.done(function(data) {
        time_ajax=setTimeout(function(){
                clearTimeout(time_ajax);
                $("#category-content").html(data);
                changeActiveClass(id);
                $('html, body').animate({ scrollTop: $('#category-content').offset().top }, 'slow');
            }, 500);

    });
});

$("#is_slide").click(function(event) {

    if($("#is_slide").is(":checked")) {
       $(".slide_texts").show(); 
    }else{
        $(".slide_texts").hide();
    }

});


//when html is rendered
jQuery(document).ready(function() {
    
    validateForm($('#emailForm'));
    
    maskTimer();
    
    // Show or hide the sticky footer button
    $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.backtotop, .mailus, .phoneus, .backtotop-large, .mailus-large').fadeIn('300');
            } else {
                $('.backtotop, .mailus, .phoneus, .backtotop-large, .mailus-large').fadeOut('300');
            }
    });
    
    $("a.backtotop").click(function(event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: $('#totop').offset().top }, 'medium');
    });
});

function setMenuEffect(c) {
    $(c).mouseenter(function() {
        $(c).fadeTo('fast', 0.7);
        $(c).animate({height:130},100);
    });
    $(c).mouseleave(function() {
        $(c).fadeTo('fast', 1);
        $(c).animate({height:96},100);
    });   
}

function menuEffect() {
    setMenuEffect('.color-1');
    setMenuEffect('.color-2');
    setMenuEffect('.color-3');
};