<?php
/* 
Plugin Name: Eesti Muotopeite Ajax
Plugin URI: http://elesan.ee
Description: Plugin for displaying frontpage content over AJAX
Author: Sander Mets
Version: 1.0 
Author URI: http://elesan.ee
*/

// embed the javascript file that makes the AJAX request
//wp_enqueue_script( 'jq', get_template_directory_uri() . '/javascripts/vendor/jquery.js', null, '4.0', true );
wp_enqueue_script( 'jq', '//ajax.googleapis.com/ajax/libs/jquery/1.10.0/jquery.min.js', null, '4.0', true );
wp_enqueue_script( 'jqui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation', get_template_directory_uri() . '/javascripts/foundation/foundation.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation1', get_template_directory_uri() . '/javascripts/foundation/foundation.joyride.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation2', get_template_directory_uri() . '/javascripts/foundation/foundation.forms.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation3', get_template_directory_uri() . '/javascripts/foundation/foundation.dropdown.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation4', get_template_directory_uri() . '/javascripts/foundation/foundation.cookie.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation5', get_template_directory_uri() . '/javascripts/foundation/foundation.alerts.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation6', get_template_directory_uri() . '/javascripts/foundation/foundation.reveal.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation7', get_template_directory_uri() . '/javascripts/foundation/foundation.clearing.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation8', get_template_directory_uri() . '/javascripts/foundation/foundation.magellan.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation9', get_template_directory_uri() . '/javascripts/foundation/foundation.tooltips.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation10', get_template_directory_uri() . '/javascripts/foundation/foundation.orbit.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation11', get_template_directory_uri() . '/javascripts/foundation/foundation.topbar.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation12', get_template_directory_uri() . '/javascripts/foundation/foundation.section.js', 'jq', '4.0', true );
wp_enqueue_script( 'foundation13', get_template_directory_uri() . '/javascripts/foundation/foundation.placeholder.js', 'jq', '4.0', true );
wp_enqueue_script( 'jrotate', get_template_directory_uri() . '/javascripts/plugins/jQrotate.js', 'jq', '1.0', true );
wp_enqueue_script( 'flexs', get_template_directory_uri() . '/javascripts/plugins/jquery.flexslider-min.js', 'jrotate', '2.0', true );
wp_enqueue_script( 'my-ajax-request', plugin_dir_url( __FILE__ ) . 'js/ajax.js', 'flexs', '1.0', true );



wp_localize_script( 'my-ajax-request', 'MyAjax', array(
	// URL to wp-admin/admin-ajax.php to process the request
	'ajaxurl'          => admin_url( 'admin-ajax.php' ),

	// generate a nonce with a unique ID "myajax-post-comment-nonce"
	// so that you can check it later when an AJAX request is sent
	//'postCommentNonce' => wp_create_nonce( 'myajax-post-comment-nonce' ),
	)
);
if(!isset($_REQUEST['action'])) $_REQUEST['action'] = '';
// this hook is fired if the current viewer is not logged in
do_action( 'wp_ajax_nopriv_' . $_REQUEST['action'] );

if(!isset($_POST['action'])) $_POST['action'] = '';
// if logged in:
do_action( 'wp_ajax_' . $_POST['action'] );
add_action( 'wp_ajax_nopriv_myajax-submit', 'myajax_submit' );
add_action( 'wp_ajax_myajax-submit', 'myajax_submit' );

function myajax_submit() {
        
        // check to see if the submitted nonce matches with the
	// generated nonce we created earlier
	//if ( ! wp_verify_nonce( $nonce, 'myajax-post-comment-nonce' ) )
	//	die ( 'Busted!');
        
        /*
	// get the submitted parameters
	$postID = $_POST['postID'];
        
	// generate the response
	$response = json_encode( array( 'success' => true ) );
 
	// response output
	header( "Content-Type: application/json" );
	echo $response;
        */
	// IMPORTANT: don't forget to "exit"
        include 'ajax.php';
	exit;
}