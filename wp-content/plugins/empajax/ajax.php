<?php
    $current_language = $_POST['lang'];

    $title = 'title_'.$current_language; 
    $text = 'text_'.$current_language;
    $paths=wp_upload_dir();
    $cat_id = intval($_POST['id']);
    $text_content = emp_get_posts_by_cat($cat_id);
    $images = getCategoryPicturesByIdNOSLIDE(pll_get_term($cat_id, 'en'));
?>
    <div class="bottommargin">
        <?php 
        $object_single_post = get_post($text_content[0]->ID);
        $content = apply_filters ("the_content", $object_single_post->post_content);?>
    <h4><?php echo $text_content[0]->post_title?></h4>
    <?php echo $content?>
    </div>
    <?php if(count($images)):?>
    <ul class="clearing-thumbs" data-clearing>
        <?php foreach ($images as $cimg):?>

        <li class="article-thumbs">
            <a href="<?php echo $paths['baseurl'].'/' . $cimg->name?>" class="th">
                <img data-caption="<?php echo $cimg->$title?>" src="<?php echo $paths['baseurl'].'/th_' . $cimg->name?>" class="content-img" alt="Thumbnail" />
            </a>
        </li>
        <?php endforeach; ?>
    </ul>
<?php endif;?>
