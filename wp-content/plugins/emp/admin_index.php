<style>
#table-product-group {
    border: 1px solid gray;
    border-collapse: collapse;
    width: 80%;
}
#table-product-group tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}
#table-product-group td {
    border: 1px solid gray;
    padding: 5px 10px;
    text-align: center;
}
#table-product-group th{
    color: #ccc;
    background-color: #464646;
}
</style><br />
<?php $categories = emp_get_categories_in_english();?>
<table id="table-product-group">
    <tr><th>Product group</th><th>Image count</th><th>Action</th></tr>
<?php foreach ($categories as $v):?>
    <tr>
    <td><?php echo $v->category->name?></td>
    <td><?php echo count(getCategoryPicturesById($v->category->cat_ID))?></td>
    <td><a href="<?php echo EMPADMURL?>&action=showimages&id=<?php echo $v->category->cat_ID?>" class="button-primary">Manage images</a></td>
    </tr>  
<?php endforeach; ?>
</table>
