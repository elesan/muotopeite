<?php
/* 
Plugin Name: Eesti Muotopeite
Plugin URI: http://elesan.ee
Description: Plugin for displaying frontpage content and manipulating images related to posts
Author: Sander Mets
Version: 1.0 
Author URI: http://elesan.ee
*/

error_reporting(E_ALL);
define("EMPADMURL", admin_url() . 'options-general.php?page=EMP');
define("F_PERMIS", 0776);
include ('modelimage.php');

/**
 * Admin Main
 */
function emp_admin(){
    $action = isset($_GET['action'])?$_GET['action']:'';
    switch ($action):
        case '':
            include ('admin_index.php');
            break;
        case 'showimages':
            include ('showimages.php');
            break;
    endswitch;
}

/**
 * 
 * @global type $polylang
 * @global type $wpdb
 * @return \stdClass
 */
function emp_get_categories_in_english($lang='en'){
    global $polylang; global $wpdb;
    $language = $polylang->get_language($lang);
    $languages = $polylang->get_languages_list();
    $cat_ids =  $wpdb->get_results( 
                    $wpdb->prepare( 
                            "
                            SELECT *  FROM `emp_termmeta`
                            WHERE `meta_value` = %d
                            ",
                            $language->term_id
                    ),OBJECT
                );
    //print_r($cat_ids); die;
    $results = array();
    foreach ($cat_ids as $key => $value) {
        $obj = new stdClass();
        $category = get_category( $value->term_id);
        $cat = $category->cat_ID;
        $obj->category = $category;
        $results[] = $obj;
    }
    return $results;
}

/**
 * Admin config
 */
function emp_admin_actions() {  
    add_options_page("Eesti Muotopeite", "Eesti Muotopeite", 1, "EMP", "emp_admin");
}

//WORDPRESS HOOK
add_action('admin_menu', 'emp_admin_actions');

/**
 * 
 * @return type
 * @throws Exception
 */
function emp_file_upload(){
    //Array ( [path] => /var/www/wptest/wp-content/uploads/2013/05 
    //[url] => http://localhost/wptest/wp-content/uploads/2013/05 
    //[subdir] => /2013/05 
    //[basedir] => /var/www/wptest/wp-content/uploads 
    //[baseurl] => http://localhost/wptest/wp-content/uploads [error] => )
    $paths=wp_upload_dir();
    $allowedExts = array("gif", "jpeg", "jpg", "png");
    $extension = strtolower(end(explode(".", $_FILES["file"]["name"])));

    if ((($_FILES["file"]["type"] == "image/gif")
        || ($_FILES["file"]["type"] == "image/jpeg")
        || ($_FILES["file"]["type"] == "image/jpg")
        || ($_FILES["file"]["type"] == "image/pjpeg")
        || ($_FILES["file"]["type"] == "image/x-png")
        || ($_FILES["file"]["type"] == "image/png"))
        && ($_FILES["file"]["size"] <  emp_detectMaxUploadFileSize() )
        && in_array($extension, $allowedExts)){
        if ($_FILES["file"]["error"] > 0){
            throw new Exception("File error: Return Code: " . $_FILES["file"]["error"] );
        }
        else{
            $filename = urlencode(uniqid().".".$extension /*. $_FILES["file"]["name"]*/);
            $th_uri = $paths['basedir']. "/th_" . $filename;
            $fileuri = $paths['basedir']. "/" . $filename;
            
            $r['name'] = $filename;
            $r['type'] = $_FILES["file"]["type"];
            
            
            if(isset($_POST['is_slide'])){
                if(emp_createCorrectedSLIDE($_FILES["file"]["tmp_name"], $fileuri, $_FILES["file"]["type"])){
                    if(!emp_createThumbNail($fileuri, $th_uri, $_FILES["file"]["type"])){
                        @unlink($fileuri);
                        throw new Exception('Could not create or save thumbnail.');
                    }
                }
                else {
                    throw new Exception('Could not save file. Check folder permissions!');
                } 
            }
            else{
                if(emp_createCorrectedIMG($_FILES["file"]["tmp_name"], $fileuri, $_FILES["file"]["type"], 480, 100)){
                    if(!emp_createThumbNail($fileuri, $th_uri, $_FILES["file"]["type"])){
                        @unlink($fileuri);
                        throw new Exception('Could not create or save thumbnail.');
                    }
                }
                else {
                    throw new Exception('Could not save file. Check folder permissions!');
                }
            }
          return $r;
        }
    }
    else{
      throw new Exception('File error - check file type and size!<pre>'. print_r($_FILES) . '</pre>' );
    }  
}

/**
 * 
 */
function emp_insert_image(){
    
    if(isset($_GET['id'])){
        try{
            $values = emp_file_upload();
            $v = array_merge($values, $_POST);
            $img = new EmpImage;
            $img->bind($v)->insertImage();
            emp_clear_url();
        }
        catch(Exception $e){
            emp_showMessage($e->getMessage(), true);}
        
    }
}

function emp_get_posts_by_cat($postcat){
    $q = new WP_Query(array( 'cat' => $postcat));
    $posts = array();
    if($q->have_posts()){
        // foreach post found
        while($q->have_posts()){
            return $q->posts;
            // code for displaying each post goes here
        }
        // cleanup after the WP_Query, reset the post data
        wp_reset_postdata();
        return $posts;
    } else {
      echo 'no posts were found!';
    }
}
/**
 * 
 * @global type $polylang
 * @param type $cat
 * @return type
 */
function getRelatedPostsByCatID($cat){
    $relatedposts = emp_get_posts_by_cat($cat);
    return $relatedposts;
}

/**
 * 
 * @return type
 */
function getCategoryPictures(){
    $cat = (int) $_GET['id'];
    return getCategoryPicturesById($cat);
}

/**
 * 
 * @global type $wpdb
 * @param type $cat
 * @return type
 */
function getCategoryPicturesById($cat){
    global $wpdb;
    $images =  $wpdb->get_results( 
                    $wpdb->prepare( 
                            "
                            SELECT emp_images.id_images, emp_images.name, 
                            emp_posts_images.cat_ID, emp_images.title_et, 
                            emp_images.title_en, emp_images.title_ru, 
                            emp_images.is_slide
                            FROM  `emp_posts_images` 
                            INNER JOIN emp_images ON emp_images.`id_images` = emp_posts_images.id_image
                            WHERE  `cat_ID` = %d
                            GROUP BY emp_posts_images.id_image
                            ",
                            $cat
                    ),OBJECT
                );
    return $images;
}

function emp_getSLIDES(){
    global $wpdb;
    $images =  $wpdb->get_results( 
           
                $wpdb->prepare( 
                        "
                        SELECT emp_images.id_images, emp_images.name, 
                        emp_posts_images.cat_ID, emp_images.title_et, 
                        emp_images.title_en, emp_images.title_ru, 
                        emp_images.is_slide, emp_images.text_et, 
                        emp_images.text_en, emp_images.text_ru
                        FROM  `emp_posts_images` 
                        INNER JOIN emp_images 
                        ON emp_images.`id_images` = emp_posts_images.id_image
                        WHERE is_slide = 1
                        GROUP BY emp_posts_images.id_image
                        "
                ),OBJECT
            );
    return $images;
}

function getCategoryPicturesByIdNOSLIDE($cat){
    global $wpdb;
    $images =  $wpdb->get_results( 
           
                $wpdb->prepare( 
                        "
                        SELECT emp_images.id_images, emp_images.name, 
                        emp_posts_images.cat_ID, emp_images.title_et, 
                        emp_images.title_en, emp_images.title_ru, 
                        emp_images.is_slide
                        FROM  `emp_posts_images` 
                        INNER JOIN emp_images 
                        ON emp_images.`id_images` = emp_posts_images.id_image
                        WHERE  `cat_ID` = %d AND is_slide = 0
                        GROUP BY emp_posts_images.id_image
                        ",
                        $cat
                ),OBJECT
            );
    return $images;
}


function emp_createCorrectedSLIDE($src, $tpath, $type, $n_width = 480, $n_height = 310, $quality = 100){
    $tsrc = $tpath;
    $add = $src;
    if($type=="image/png"){
       $im=ImageCreateFromPNG($add); 
       $width=ImageSx($im);              // Original picture width is stored
       $height=ImageSy($im);             // Original picture height is stored

       //$n_width = floor($width * ($n_height/$height));
        /* make sure that dimensions width related to height are within range and vice versa */
       /*
       if(($n_height > 10* $n_width) or ($n_height > 10 * $n_width)){
            return false;
       }
       */
       $newimage=imagecreatetruecolor($n_width,$n_height);                 
       imageCopyResized($newimage,$im,0,0,0,0,$n_width,$n_height,$width,$height);
       ImagePng($newimage,$tsrc, $quality);
       chmod("$tsrc",F_PERMIS);
    }
    elseif ($type=="image/gif")
    {
        $im=ImageCreateFromGIF($add);
        $width=ImageSx($im);              // Original picture width is stored
        $height=ImageSy($im);                  // Original picture height is stored
        
        //$n_width = floor($width * ($n_height/$height));

        /* make sure that dimensions width related to height are within range and vice versa 
        if(($n_height > 10* $n_width) or ($n_height > 10* $n_width)){
            return false;
        }  */
        $newimage=imagecreatetruecolor($n_width,$n_height);
        imageCopyResized($newimage,$im,0,0,0,0,$n_width,$n_height,$width,$height);
        ImageGIF($newimage,$tsrc);
        chmod("$tsrc",F_PERMIS);
    }
    
    elseif($type=="image/jpeg" or $type=="image/jpg"){
        $im=ImageCreateFromJPEG($add); 
        $width=ImageSx($im);              // Original picture width is stored
        $height=ImageSy($im);             // Original picture height is stored
        //$n_width = floor($width * ($n_height/$height));
        /* make sure that dimensions width related to height are within range and vice versa 
        if(($n_height > 10* $n_width) or ($n_height > 10* $n_width)){
            return false;
        }  */
        $newimage=imagecreatetruecolor($n_width,$n_height);                 
        $x1 = imageCopyResized($newimage,$im,0,0,0,0,$n_width,$n_height,$width,$height);
        $x2 = ImageJpeg($newimage,$tsrc, $quality);
        $x3 = chmod("$tsrc",F_PERMIS);
        return true;
    } 
}
/**
 * http://stackoverflow.com/questions/10360788/create-thumbnail-creation-of-png-jpg-and-gif-images
 * @param type $src
 * @param type $tpath
 * @param type $type
 * @param type $n_width -> width we wish to have
 * @param type $quality -> 0 - 100
 * @return boolean
 */
function emp_createThumbNail($src, $tpath, $type, $n_width = 200, $n_height = 150, $quality = 100){
    $tsrc = $tpath;
    $add = $src;
    if($type=="image/png"){
       $im=ImageCreateFromPNG($add); 
       $width=ImageSx($im);              // Original picture width is stored
       $height=ImageSy($im);             // Original picture height is stored
       //$n_height = floor($height*($n_width/$width));

        /* make sure that dimensions width related to height are within range and vice versa 
        if(($n_height > 10* $n_width) or ($n_height > 10* $n_width)){
            return false;
        }  */
       $newimage=imagecreatetruecolor($n_width,$n_height);                 
       imageCopyResized($newimage,$im,0,0,0,0,$n_width,$n_height,$width,$height);
       ImagePng($newimage,$tsrc, $quality);
       chmod("$tsrc",F_PERMIS);
    }
    elseif ($type=="image/gif")
    {
        $im=ImageCreateFromGIF($add);
        $width=ImageSx($im);              // Original picture width is stored
        $height=ImageSy($im);                  // Original picture height is stored
        
        //$n_height = floor($height*($n_width/$width));

        /* make sure that dimensions width related to height are within range and vice versa
        if(($n_height > 10* $n_width) or ($n_height > 10* $n_width)){
            return false;
        }   */
        $newimage=imagecreatetruecolor($n_width,$n_height);
        imageCopyResized($newimage,$im,0,0,0,0,$n_width,$n_height,$width,$height);
        ImageGIF($newimage,$tsrc);
        chmod("$tsrc",F_PERMIS);
    }
    
    elseif($type=="image/jpeg" or $type=="image/jpg"){
        $im=ImageCreateFromJPEG($add); 
        $width=ImageSx($im);              // Original picture width is stored
        $height=ImageSy($im);             // Original picture height is stored
        //$n_height = floor($height*($n_width/$width));
        /* make sure that dimensions width related to height are within range and vice versa 
        if(($n_height > 10* $n_width) or ($n_height > 10* $n_width)){
            return false;
        }  */
        $newimage=imagecreatetruecolor($n_width,$n_height);                 
        $x1 = imageCopyResized($newimage,$im,0,0,0,0,$n_width,$n_height,$width,$height);
        $x2 = ImageJpeg($newimage,$tsrc, $quality);
        $x3 = chmod("$tsrc",F_PERMIS);
        return true;
    } 
}

function emp_createCorrectedIMG($src, $tpath, $type, $n_height, $quality){
    $tsrc = $tpath;
    $add = $src;
    if($type=="image/png"){
       $im=ImageCreateFromPNG($add); 
       $width=ImageSx($im);              // Original picture width is stored
       $height=ImageSy($im);             // Original picture height is stored

       $n_width = floor($width * ($n_height/$height));
        /* make sure that dimensions width related to height are within range and vice versa*/
       if(($n_height > 10* $n_width) or ($n_height > 10 * $n_width)){
            return false;
       } 
       
       $newimage=imagecreatetruecolor($n_width,$n_height);                 
       imageCopyResized($newimage,$im,0,0,0,0,$n_width,$n_height,$width,$height);
       ImagePng($newimage,$tsrc, $quality);
       chmod("$tsrc",F_PERMIS);
    }
    elseif ($type=="image/gif")
    {
        $im=ImageCreateFromGIF($add);
        $width=ImageSx($im);              // Original picture width is stored
        $height=ImageSy($im);                  // Original picture height is stored
        
        $n_width = floor($width * ($n_height/$height));

        /* make sure that dimensions width related to height are within range and vice versa */
        if(($n_height > 10* $n_width) or ($n_height > 10* $n_width)){
            return false;
        }   
        $newimage=imagecreatetruecolor($n_width,$n_height);
        imageCopyResized($newimage,$im,0,0,0,0,$n_width,$n_height,$width,$height);
        ImageGIF($newimage,$tsrc);
        chmod("$tsrc",F_PERMIS);
    }
    
    elseif($type=="image/jpeg" or $type=="image/jpg"){
        $im=ImageCreateFromJPEG($add); 
        $width=ImageSx($im);              // Original picture width is stored
        $height=ImageSy($im);             // Original picture height is stored
        $n_width = floor($width * ($n_height/$height));
        /* make sure that dimensions width related to height are within range and vice versa */
        if(($n_height > 10* $n_width) or ($n_height > 10* $n_width)){
            return false;
        }  
        $newimage=imagecreatetruecolor($n_width,$n_height);                 
        $x1 = imageCopyResized($newimage,$im,0,0,0,0,$n_width,$n_height,$width,$height);
        $x2 = ImageJpeg($newimage,$tsrc, $quality);
        $x3 = chmod("$tsrc",F_PERMIS);
        return true;
    } 
}

/**
 * 
 */
function emp_delete_image(){
        $img = new EmpImage;
        $image = $img->bind($_GET, false)->getImageById();
        $paths=wp_upload_dir();
        @unlink($paths['basedir']. "/th_" . $image->name);
        @unlink($paths['basedir']. "/" . $image->name);
        $img->deleteImage();
}

/**
 * 
 * @return type
 */
function emp_get_image_by_id(){
    $img = new EmpImage;
    return $img->bind($_GET, false)->getImageById();
}

/**
 * 
 */
function emp_update_image(){
    $img = new EmpImage;
    $img->bind($_POST, false)->updateImage();    
}

/**
 * 
 */
function emp_clear_url(){
    echo '
        <script type="text/javascript">
        <!--
        window.location = "'.EMPADMURL.'&action=showimages&id='.((int) $_GET['id']).'"
        //-->
        </script>        
    ';
}

/**
 * 
 * @return string
 */
function emp_curPageURL() {
    $isHTTPS = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
    $port = (isset($_SERVER["SERVER_PORT"]) && ((!$isHTTPS && $_SERVER["SERVER_PORT"] != "80") || ($isHTTPS && $_SERVER["SERVER_PORT"] != "443")));
    $port = ($port) ? ':'.$_SERVER["SERVER_PORT"] : '';
    $url = ($isHTTPS ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$port.$_SERVER["REQUEST_URI"];
    return $url;
}

/**
* Detects max size of file cab be uploaded to server
*
* Based on php.ini parameters "upload_max_filesize", "post_max_size" &
* "memory_limit". Valid for single file upload form. May be used
* as MAX_FILE_SIZE hidden input or to inform user about max allowed file size.
* RULE memory_limit > post_max_size > upload_max_filesize
* http://php.net/manual/en/ini.core.php : 128M > 8M > 2M
* Sets max size of post data allowed. This setting also affects file upload. 
* To upload large files, this value must be larger than upload_max_filesize. 
* If memory limit is enabled by your configure script, memory_limit also 
* affects file uploading. Generally speaking, memory_limit should be larger 
* than post_max_size. When an integer is used, the value is measured in bytes. 
* Shorthand notation, as described in this FAQ, may also be used. If the size 
* of post data is greater than post_max_size, the $_POST and $_FILES 
* superglobals are empty. This can be tracked in various ways, e.g. by passing 
* the $_GET variable to the script processing the data, i.e. 
* <form action="edit.php?processed=1">, and then checking 
* if $_GET['processed'] is set.
* memory_limit > post_max_size > upload_max_filesize
* @author Paul Melekhov edited by lostinscope
* @return int Max file size in bytes
*/
function emp_detectMaxUploadFileSize(){
    /**
    * Converts shorthands like "2M" or "512K" to bytes
    *
    * @param $size
    * @return mixed
    */
    $normalize = function($size) {
        if (preg_match('/^([\d\.]+)([KMG])$/i', $size, $match)) {
            $pos = array_search($match[2], array("K", "M", "G"));
            if ($pos !== false) {
                $size = $match[1] * pow(1024, $pos + 1);
            }
        }
        return $size;
    };
    $max_upload = $normalize(ini_get('upload_max_filesize'));
    
    $max_post = (ini_get('post_max_size') == 0) ? 
            function(){throw new Exception('Check Your php.ini settings');}
            : $normalize(ini_get('post_max_size'));
            
    $memory_limit = (ini_get('memory_limit') == -1) ? 
            $max_post : $normalize(ini_get('memory_limit'));
    
    if($memory_limit < $max_post || $memory_limit < $max_upload)
        return $memory_limit;
    
    if($max_post < $max_upload)
        return $max_post;
    
    $maxFileSize = min($max_upload, $max_post, $memory_limit);
    return $maxFileSize;
}

/**
 * 
 * @param type $message
 * @param type $errormsg
 */
function emp_showMessage($message, $errormsg = false){
    echo ($errormsg) ? '<div id="message" class="error">'
            : '<div id="message" class="updated fade">';
    echo "<p><strong>$message</strong></p></div>";
}

function emp_get_frontpage_subtitles(){
    
}
