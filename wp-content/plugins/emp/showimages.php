<?php set_time_limit(300)?>
<?php if(isset($_POST['submitfile'])){ emp_insert_image();}?>
<?php if(isset($_POST['update'])){ 
    emp_update_image(); emp_clear_url();}?>
<?php if(isset($_GET['del_img'])){
    emp_delete_image(); emp_clear_url();}?>
<?php $paths=wp_upload_dir()?>
<?php $current_category = get_category(( int ) $_GET['id']); ?>
<style>
#table-product-images {
    border: 1px solid gray;
    border-collapse: collapse;
    width: 80%;
    margin-left: 15px;
}
#table-product-images tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}
#table-product-images td {
    border: 1px solid gray;
    padding: 5px 10px;
    text-align: center;
}
#table-product-images th{
    color: #ccc;
    background-color: #464646;
    padding: 4px;
    clear: left;
}
.iwidth{width: 100%;}
.iwidthf{width: 99%;}
.table-footer{
    color: #ccc;
    background-color: #464646;
    padding: 4px;
    clear: right;
}

</style>
<div class="wrap">
<form name="emp_form" id="emp-form" method="post" enctype="multipart/form-data" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
        <table id="table-product-images">
            <tr><th><a href="<?php echo EMPADMURL?>" class="button-primary" style="float: left"><< Back to categories</a></th><th>Current category: <?php echo $current_category->name?> (images: <?php echo count(getCategoryPicturesById($current_category->cat_ID))?>)</th></tr>
            <?php if(isset($_GET['update_img'])): $img = emp_get_image_by_id()?>
                <tr><td>Title in english</td><td><input type="text" name="title_en" value="<?php echo $img->title_en?>" class="iwidth required"></td></tr>
                <tr><td>Title in estonian</td><td><input type="text" name="title_et" value="<?php echo $img->title_et?>" class="iwidth required"></td></tr>
                <tr><td>Title in russian</td><td><input type="text" name="title_ru" value="<?php echo $img->title_ru?>" class="iwidth required"><input type="hidden" name="id_images" value="<?php echo $img->id_images?>"></td></tr>
                <?php if($img->is_slide == 1):?>
                <tr>
                <td>Text in english</td>
                <td>
                    <textarea rows="4" cols="50" name="text_en"><?php echo $img->text_en?></textarea>
                </td>
                </tr>
                
                <tr>
                <td>Text in estonian</td>
                <td>
                    <textarea rows="4" cols="50" name="text_et"><?php echo $img->text_et?></textarea>
                </td>
                </tr>
                
                <tr>
                <td>Text in russian</td>
                <td>
                    <textarea rows="4" cols="50" name="text_ru"><?php echo $img->text_ru?></textarea>
                </td>
                </tr>
                <?php endif;?>
                
                <tr><td></td><td><input type="submit" name="update" class="button-primary" value="<?php echo 'UPDATE' ?>" /></td></tr>
            <?php else:?>
                <tr><td>Title in english</td><td><input type="text" name="title_en" class="iwidth required"></td></tr>
                <tr><td>Title in estonian</td><td><input type="text" name="title_et" class="iwidth required"></td></tr>
                <tr><td>Title in russian</td><td><input type="text" name="title_ru" class="iwidth required"></td></tr>
                <tr><td>MAX file size <?php echo (emp_detectMaxUploadFileSize() / (1024*1024)).'MB' ?><br />gif, jpeg, jpg, png</td><td><input type="file" name="file" id="file" class="iwidthf required"></td></tr>
                <tr><td>Is Main Slide</td><td><input type="checkbox" name="is_slide" id="is_slide" value="1"/></td></tr>
                <tr class="slide_texts" style="display: none">
                <td>Text in english</td>
                <td>
                    <textarea rows="4" cols="50" name="text_en"></textarea>
                </td>
                </tr>
                
                <tr class="slide_texts" style="display: none">
                <td>Text in estonian</td>
                <td>
                    <textarea rows="4" cols="50" name="text_et"></textarea>
                </td>
                </tr>
                
                <tr class="slide_texts" style="display: none">
                <td>Text in russian</td>
                <td>
                    <textarea rows="4" cols="50" name="text_ru"></textarea>
                </td>
                </tr>
                
                
                <tr><td colspan="2" class="table-footer"><input type="submit" name="submitfile" class="button-primary" style="float: right" value="<?php echo 'ADD NEW IMAGE' ?>" /></td></tr>
            <?php endif;?>
        </table>
</form>
</div><br />
<?php $pictures = getCategoryPictures()?>
<?php if(count($pictures)>0):?>
<table id="table-product-images">
    <tr>
        <th>Image thumbnail</th>
        <th>Title EN</th>
        <th>Title ET</th>
        <th>Title RU</th>
        <th>IS SLIDE</th>
        <th>ACTIONS</th>
    </tr>
<?php foreach ($pictures as $v):?>
    <tr>
        <td><a href="<?php echo $paths['baseurl']. "/" . $v->name?>" target="_blank"><img src="<?php echo $paths['baseurl']. "/th_" . $v->name?>"/></a></td>
        <td><?php echo $v->title_en?></td>
        <td><?php echo $v->title_et?></td>
        <td><?php echo $v->title_ru?></td>
        <td><?php echo $v->is_slide ? "YES <br />(see update)" : "NO"?></td>
        <td>
            <?php if(!isset($_GET['update_img'])):?>
            <a href="<?php echo emp_curPageURL()?>&update_img=1&id_images=<?php echo $v->id_images?>" class="button-primary">UPDATE</a><br /><br />
            <a href="<?php echo emp_curPageURL()?>&del_img=1&id_images=<?php echo $v->id_images?>" class="button-primary">DELETE</a>
            <?php endif?>    
        </td>
    </tr>
<?php endforeach; ?>
</table>
<?php else:?>
<p style="font-weight: bold">No images added yet. Please use form above to add.</p>
<?php endif; ?>

