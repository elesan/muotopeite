<?php
class EmpImage {
    
    public $type;
    public $name;
    
    public $title_et;
    public $title_en;
    public $title_ru;
    public $id_images;
    public $is_slide = 0;

    public $text_et = NULL;
    public $text_en = NULL;
    public $text_ru = NULL;
    
    public $db;
    
    public function __construct() {
        global $wpdb;
        $this->db = $wpdb;
    }
    
  /**
   * Set the property directly.
   *
   * @param  string $name
   * @param  mixed  $value
   */
  public function __set($name, $value){
      if (property_exists($this, $name)) {
          $this->$name = $value;
      }        
  }

  /**
   * 
   * Creates model
   * @param type $array
   */
  public function bind($values = array(), $no_pk = true){
    if($no_pk && isset($values['id_images'])){
                unset($values['id_images']); 
    }
    if(is_array($values ) and count($values) > 0){
        foreach ($values as $key => $value) {
            $this->__set($key, $value);
        }
    }
    return $this;
  }
  
  public function insertImage(){
      
    $cat = (int) $_GET['id'];
    global $polylang;

    $this->id_images = $this->getLastId();
    $this->db->query( 
        $this->db->prepare( 
                "
                INSERT INTO `emp_images` 
                (`id_images`, `name`, `title_et`, `title_en`, 
                `title_ru`, `type`, `is_slide`, `text_en`, `text_et`, `text_ru`) 
                VALUES (%d, %s, %s, %s, %s, %s, %d, %s, %s, %s);
                ",
                $this->id_images, $this->name, $this->title_et, 
                $this->title_en, $this->title_ru, $this->type, $this->is_slide,
                $this->text_en, $this->text_et, $this->text_ru
        )
    );


    $this->db->query( 
        $this->db->prepare( 
                "
                INSERT INTO `emp_posts_images` 
                (`id_image`,  `cat_ID`) VALUES (%d,%d);
                ",
                $this->id_images, $cat
        )
    );    
    
  }
  
  public function getLastId(){
      $id = $this->db->get_var( "SELECT  `id_images` 
                                 FROM  `emp_images` 
                                 ORDER BY id_images DESC LIMIT 1" );
      if($id) $id++;
      else $id = 1;
      return $id;
  }
  
  public function getImageById(){
      $image = $this->db->get_row(
              $this->db->prepare( 
                    "
                    SELECT * 
                    FROM  `emp_images` 
                    WHERE  `id_images` = %d
                    ",
                    $this->id_images
                )
              
              );
      return $image;
  }
  
  public function deleteImage(){
        $this->db->query( 
            $this->db->prepare( 
                    "
                    DELETE FROM `emp_images` 
                    WHERE `emp_images`.`id_images` = %d;
                    ",
                    $this->id_images
            )
        ); 
  }
  public function updateImage(){
        $this->db->query( 
            $this->db->prepare( 
                    "
                    UPDATE `emp_images` 
                    SET `title_et` =  %s, `title_en` =  %s, `title_ru` =  %s, 
                        `text_et` =  %s, `text_en` =  %s, `text_ru` =  %s
                    WHERE `id_images` = %d;
                    ",
                    $this->title_et, $this->title_en, 
                    $this->title_ru, $this->text_et, 
                    $this->text_en, $this->text_ru,
                    $this->id_images
            )
        ); 
  }
}
